<?xml version="1.0" encoding="UTF-8"?>

<!--
 =================================================================
  Licensed Materials - Property of IBM

  WebSphere Commerce

  (C) Copyright IBM Corp. 2007, 2011 All Rights Reserved.

  US Government Users Restricted Rights - Use, duplication or
  disclosure restricted by GSA ADP Schedule Contract with
  IBM Corp.
 =================================================================
-->

<!--
 ===============================================================================
 This Ant file is the sample source extraction script from Subversion.
 ===============================================================================
-->
<project name="extract-svn" default="all">

	<available property="found.${ant.project.name}.properties"
	           file="${basedir}/${ant.project.name}.properties"
	           type="file" />
	<fail message="${basedir}/${ant.project.name}.properties does not exist."
	      unless="found.${ant.project.name}.properties" />
	<property file="${basedir}/${ant.project.name}.properties" />

	<encodeProperties file="${ant.project.name}.private.properties" />
	<decodeLoadProperties file="${ant.project.name}.private.properties" />

	<!--
	 Runs the source extraction process.
	-->
	<target name="all">
		<!--
		 This sample script uses the <svn> task from Tigris:
		 http://subclipse.tigris.org/svnant.html
		 
		 For more info about the <svn> task, refer to:
		 http://subclipse.tigris.org/svnant/svn.html
		-->
		<typedef resource="org/tigris/subversion/svnant/svnantlib.xml">
			<classpath>
				<fileset dir="${svn.ant.lib.dir}" includes="*.jar" />
			</classpath>
		</typedef>
		<svn svnkit="false" javahl="false">
		<if>
			<istrue value="${extract.update.mode}" />
			<then>
				<svn failonerror="true"
				     username="${svn.user}"
				     password="${svn.password}">
					<update dir="${source.dir}" />
				</svn>
			</then>
			<else>
				<svn failonerror="true"
				     username="${svn.user}"
				     password="${svn.password}">
					<checkout url="${svn.url}"
					          destPath="${source.dir}"
					          revision="${svn.revision}" />
				</svn>
			</else>
		</if>
	</target>

</project>
